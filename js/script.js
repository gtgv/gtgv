// Get params from url
$.extend({
  getUrlVars: function() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name) {
    return $.getUrlVars()[name];
  }
});

// Load all javascript features
// Defined as a method to ensure we can reapply it in some contexts, for example ajax
$(document).ready(function() {
  media = document.getElementById("media");
  text = document.getElementById("text");

  var url = decodeURIComponent($.getUrlVar('url'));
  if (url == 'undefined' || url == '') {
    $('#output').hide();
    $('#media-c').hide();
    $('#shortcuts').hide();
  } else {
    $(text).focus();
    $('#input').hide();
    $('#url').attr('value', url);
    set_shortcuts($(text), $('#shortcuts_help'));

    media.src = url;
  }
});
