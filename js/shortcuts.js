// Also see http://v2v.cc/~j/jquery.srt/

function play_pause() {
  if (media.paused) {
    // Go back some time in the past
    media.currentTime = (media.currentTime-1 > 0) ? media.currentTime-0.5 : 0;
    media.play();
  } else {
    media.pause();
  }
}

var reactionTime = 0.5;

function insert_timestamp() {
  var time = media.currentTime;
  if (!media.paused) {
    var time = time - reactionTime/1000;
  }

  var hours = Math.floor(time/3600);
  var minutes = Math.floor(time%3600/60);
  var seconds = time%3600%60;
  var t = hours+':'+minutes+':'+seconds;
  $(text).insertAtCaret(t.replace('.', ',').substr(0, 12));
}

function handleKey(event) {
  if (event.ctrlKey || event.altKey) {
    var command = $('li[data-keycode="'+event.keyCode+'"]').attr('data-command');

    if (command != null) {
      eval(command);
    }
  } else {
    if ($('#characters_count').text() > text.value.length + 15
        || $('#characters_count').text() < text.value.length - 15) {
      media.pause();
      $('#characters_count').text(text.value.length);
    }
  }
}

function set_shortcut(element, event) {
  element.parent().attr('data-keycode', event.keyCode);
  element.parent().find('abbr.value').text(element.attr('value'));
  //console.log(element.parent().attr('data-keycode'));
}

function set_shortcuts(text, doc_node) {
  text.keyup(function(event) {
    handleKey(event);
  });

  $('#shortcut_editor_toggler').click(function() {
    $('#shortcuts_help input').toggle();
    $('#shortcuts_help .value').toggle();
  });

  $('#shortcuts_help input').keyup(function(event) {
    set_shortcut($(this), event);
  });

  if (Modernizr.localstorage) {
    // window.localStorage is available!

  } else {
    // no native support for HTML5 storage :(
    // maybe try dojox.storage or a third-party solution
  }
}
